require('dotenv').config();
const path = require('path');
const webpack = require('webpack');

// Подключаемые плагины
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

////////////////////////////////////////////////////////////////////////////////////////////////////
// Конфигурация
const __DEV__ = process.env.NODE_ENV !== 'production';
const DEBUG = JSON.stringify(process.env.DEBUG === 'true');
const BACKEND_API_URL = JSON.stringify(process.env.BACKEND_API_URL || 'http://localhost:8181/');
const WEBPACK_TITLE = process.env.WEBPACK_TITLE || 'Test';
const DEV_WEBPACK_HOST = process.env.DEV_WEBPACK_HOST || 'localhost';
const DEV_WEBPACK_PORT = process.env.DEV_WEBPACK_PORT || '3000';

const DEV_MOBX_LOGGER = process.env.DEV_MOBX_LOGGER ? process.env.DEV_MOBX_LOGGER : true;
const MOBX_DEVTOOLS = process.env.MOBX_DEVTOOLS ? process.env.MOBX_DEVTOOLS : false;
////////////////////////////////////////////////////////////////////////////////////////////////////

const root = process.cwd();

/** @type {webpack.Configuration} */
const config = {
    mode: __DEV__ ? 'development' : 'production',
    bail: !__DEV__,

    entry: {
        app: path.join(root, 'src/init.tsx'),
    },

    output: {
        path: path.join(__dirname, 'build'),
        publicPath: '/',
        filename: __DEV__ ? 'assets/[name].js' : 'assets/[name]-[hash:8].js',
        chunkFilename: __DEV__ ? 'assets/[name].js' : 'assets/[name]-[chunkhash:8].js',
    },

    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json', '.less'],
        modules: [root, path.resolve(root, 'src'), 'node_modules'],
    },

    plugins: [
        new CleanWebpackPlugin(['build'], {verbose: true, dry: false}),
        new HtmlWebpackPlugin({
          title: WEBPACK_TITLE,
          template: 'src/templates/index.html'
        }),
        new MiniCssExtractPlugin({filename: __DEV__ ? '[name].css' : '[name]-[hash:8].css',}),
        new webpack.ContextReplacementPlugin(/node_modules\/moment\/locale/, /ru/),
        new webpack.DefinePlugin({
          DEBUG,
          DEV_MOBX_LOGGER,
          BACKEND_API_URL,
	        MOBX_DEVTOOLS,
        }),
    ],

    module: {
        strictExportPresence: true,
        rules: [
            {
                test: /\.(tsx?|jsx?)$/,
                include: __DEV__
                    ? [path.resolve(__dirname, 'src')]
                    : [path.resolve(__dirname, 'src'), path.resolve(__dirname, 'node_modules')],
                use: [
                    {
                        loader: 'babel-loader',
                    },
                    'ts-loader?transpileOnly=true',
                ],
            },
            {
                test: /\.(jpe?g|png|gif|svg|eot|ttf|woff2?)$/,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 8192,
                        name: `assets/media/[name]${__DEV__ ? '' : '-[hash:8]'}.[ext]`
                    }
                }
            },
            {
                test: /\.svg$/,
                use: 'svg-sprite-loader',
            },
            {
                test: /\.less$/,
                use: [
                    __DEV__ ? 'style-loader' : MiniCssExtractPlugin.loader,
                    `css-loader?minimize=${!__DEV__}`,
                    {
                        loader: 'less-loader',
                        options: {javascriptEnabled: true}
                    }
                ]
            },
            {
                test: /\.(css)$/,
                use: [
                    __DEV__ ? 'style-loader' : MiniCssExtractPlugin.loader,
                    'css-loader',
                ],
            },
        ],
    },
    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                uglifyOptions: {
                    compress: {
                        side_effects: false,
                    },
                    mangle: false,
                    output: {
                        beautify: true,
                    },
                }
            }),
        ],
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/].*\.(jsx?|tsx?)/,
                    name: "vendor",
                    chunks: "all",
                },
                styles: {
                    test: /\.(less|css)$/,
                    name: "styles",
                    chunks: "all",
                },
            },
        },
    },
};

if (__DEV__) {
    config.devServer = {
        host: DEV_WEBPACK_HOST,
        port: DEV_WEBPACK_PORT,
        contentBase: __dirname + '/public',
        historyApiFallback: true,
        disableHostCheck: true,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
            'Access-Control-Allow-Headers': 'X-Requested-With, content-type, Authorization',
        },
        stats: {
            assets: true,
            children: false,
            chunks: false,
            cached: true,
            colors: true,
            errorDetails: false,
            errors: true,
            hash: false,
            modules: false,
            publicPath: false,
            reasons: false,
            source: false,
            timings: true,
            version: false,
            warnings: true,
        },
        watchOptions: {
            ignored: /node_modules/
        },
    };
    config.output.publicPath = '/';
    config.plugins.push(new ForkTsCheckerWebpackPlugin({tslint: './tslint.json'}));
    config.devtool = 'source-map';
} else {
    config.plugins.push(
        new CompressionPlugin({test: /\.(js|css)$/i})
    );
}

module.exports = config;
