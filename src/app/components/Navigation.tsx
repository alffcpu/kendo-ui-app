import React from 'react';
import { Button, Toolbar, ToolbarItem, DropDownButton, DropDownButtonItem } from '@progress/kendo-react-buttons';
import { inject, observer } from 'mobx-react';
import { RouterStore } from 'mobx-react-router';
import { classNames } from '@progress/kendo-react-common';
import { IAuthStore } from 'app/stores/AuthStore';

interface IProps {
  router?: RouterStore;
  auth?: IAuthStore;
}

const navItems = {
  articles: {
    iconClass: 'fa-sun-o',
    title: 'Коды регионов',
  },
  login: {
    iconClass: 'fa-user',
    title: 'Форма авторизации',
  },
  errorpage: {
    iconClass: 'fa-exclamation',
    title: 'Несуществующая страница',
  }
};

@inject('router', 'auth')
@observer
export default class Navigation extends React.Component<IProps> {

  handleUserClick = ({itemIndex}) => {
    switch (itemIndex) {
      case 0:
        this.props.auth.setAuth(false);
        break;
      case 1:
        // Еще что-то :)
      break;
    }
  }

  render() {
    const {router, auth} = this.props;
    const {name, lastName, role} = auth.user;
    const {pathname} = router.location;
    return (<Toolbar className="app-navigation">
      {Object.entries(navItems).map(([page, params]) =>
        <ToolbarItem key={page}>
          <Button
            type="button"
            onClick={() => router.push(`/${page}`)}
            iconClass={classNames('fa', params.iconClass)}
            primary={pathname === `/${page}`}>{params.title}</Button>
        </ToolbarItem>
      )}
      <div className="nav-user-info">
        <DropDownButton
          iconClass="fa fa-user-o"
          text={`${name} ${lastName} (${role})`}
          onItemClick={this.handleUserClick}
        >
          <DropDownButtonItem
            text="Выйти"
            iconClass="fa fa-sign-out"
          />
        </DropDownButton>
      </div>
    </Toolbar>);
  }
}
