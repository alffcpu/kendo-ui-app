import React from 'react';
import { observer, inject } from 'mobx-react';
import { Input } from '@progress/kendo-react-inputs';
import { Button } from '@progress/kendo-react-buttons';
import { Popup } from '@progress/kendo-react-popup';
import { postAuth } from 'app/backend/login';
import { IAuthStore } from 'app/stores/AuthStore';
import sleep from 'app/utils/sleep';
import FormField from 'app/components/FormField';

interface IProps {
  auth?: IAuthStore;
}

interface IState {
  pending: boolean;
  error: boolean;
}

// для демонстрации
const defaultLogin = 'demoUser';
const defaultPassword = 'demoPassword';
const defaultRemember = false;

@inject('auth')
@observer
export default class AuthorizationForm extends React.Component<IProps, IState> {

  state = {
    pending: false,
    error: false,
  };

  formValues = {
    login: defaultLogin,
    password: defaultPassword,
    remember: defaultRemember
  };

  formCmp = null;

  handleSubmit = (e) => {
    e.preventDefault();
    const {auth} = this.props;
    this.setState({
      pending: true,
    }, async () => {
      try {
        // для демонстрации
        await sleep(1000);
        const {data} = await postAuth(this.formValues);
        this.setState({
          pending: false,
        }, () => {
          auth.setUser(data);
          auth.setAuth(true);
        });
      } catch (err) {
        this.setState({
          pending: false,
          error: true,
        }, async () => {
          await sleep(5000);
          this.setState({
            error: false,
          });
        });
      }
    });
  }

  handleInput = (event) => {
    const {name, value} = event.target;
    this.formValues[name] = value;
  }

  handleToggle = (event) => {
    const {name} = event.target;
    this.formValues[name] = !this.formValues[name];
  }

  render() {
    const {pending, error} = this.state;
    return (<div className="row">
      <div className="col-xs-12 col-sm-6 offset-sm-3">
        <div className="card border-light shadow-light login-form">
          <div className="card-block">
            <form ref={(cmp) => this.formCmp = cmp}
                className="k-form"
                onSubmit={this.handleSubmit}>
              <fieldset>
                <legend>Авторизация:</legend>
                <FormField title="Логин">
                  <Input
                    readOnly={pending}
                    onInput={this.handleInput}
                    className="full-width"
                    type="text"
                    name="login"
                    required={true}
                    pattern="[A-Za-z0-9]+"
                    defaultValue={defaultLogin}
                    validationMessage="Введите логин. Только латинские символы и цифры от 0 до 9."
                  />
                </FormField>
                <FormField title="Пароль">
                  <Input
                    readOnly={pending}
                    onInput={this.handleInput}
                    className="full-width"
                    type="password"
                    name="password"
                    required={true}
                    defaultValue={defaultPassword}
                  />
                </FormField>
                <FormField>
                  <Input
                    disabled={pending}
                    onChange={this.handleToggle}
                    type="checkbox"
                    name="remember"
                    className="k-checkbox"
                    id="remember-checkbox"
                    defaultChecked={defaultRemember}
                  />
                  <label
                    className="k-checkbox-label" htmlFor="remember-checkbox">Оставаться в системе</label>
                </FormField>
              </fieldset>
              <Button
                {...(pending ?
                  {
                    iconClass: 'fa fa-spinner fa-pulse',
                    className: 'pending',
                    disabled: true
                  } :
                  {}
                )}
                type="submit"
                primary={true}><span>Войти</span></Button>
            </form>
            <Popup
                anchor={this.formCmp}
                show={error}
                popupClass="login-form-error"
                anchorAlign={{
                  horizontal: 'center',
                  vertical: 'top'
                }}
                popupAlign={{
                  horizontal: 'center',
                  vertical: 'top'
                }}
            >
              <div onClick={() => this.setState({error: false})}>Неверный логин / пароль</div>
            </Popup>
          </div>
        </div>
      </div>
    </div>);
  }
}
