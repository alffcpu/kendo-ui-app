import React from 'react';
import { classNames } from '@progress/kendo-react-common';

interface IFormFieldProps {
  children?: any;
  title?: JSX.Element | string;
  labelClassName?: string | string[];
}

const FormField: React.SFC<IFormFieldProps> = ({children, title, labelClassName}) =>
  (<label className={classNames('k-form-field', labelClassName)}>
    {title ? <span>{title}</span> : null}
    {children}
  </label>);

export default FormField;
