import React from 'react';
import { inject, observer } from 'mobx-react';
import { IAuthStore } from 'app/stores/AuthStore';
import AuthorizationForm from 'app/components/AuthorizationForm';
import Navigation from 'app/components/Navigation';
import Routes from 'app/Routes';

interface IProps {
  auth?: IAuthStore;
}

@inject('auth')
@observer
export default class Authentication extends React.Component<IProps> {

  render() {
    const {isAuthorized} = this.props.auth;
    return isAuthorized ?
      <React.Fragment>
        <Navigation />
        <Routes />
      </React.Fragment> :
      <AuthorizationForm />;
  }
}
