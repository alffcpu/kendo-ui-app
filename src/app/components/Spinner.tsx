import React from 'react';

interface IProps {
    size?: number;
}

const Spinner: React.SFC<IProps> = ({size = 3}) =>
  <div className="spinner-container">
    <span className={`fa fa-spinner fa-pulse ${size ? `fa-${size}x` : ''}`} />
  </div>;

export default Spinner;
