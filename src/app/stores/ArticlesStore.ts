import { action, observable, set } from 'mobx';
import { postArticles } from 'app/backend/articles';

export interface IArticlesStore {
  list: IArticleEntry[];
  pending: boolean;
  error: any;
  offset: number;
  limit: number;
  total: number;
  fetchData();
  resetData();
  setValues(values);
}

export interface IArticleEntry {
  count: number;
  title: string;
  code: string;
}

export interface IArticlesResponseData {
  list: IArticleEntry[];
  status: string;
  total: number;
}

const defaultOffset = 0;
const defaultLimit = 10;

export default class ArticlesStore {
  @observable list = null;
  @observable pending = false;
  @observable error = null;
  @observable offset = defaultOffset;
  @observable limit = defaultLimit;
  @observable total = 0;

  @action async fetchData() {
    this.setValues({
      pending: true,
      error: false,
    });
    try {
      const response = await postArticles({
        offset: this.offset,
        limit: this.limit
      });

      const data: IArticlesResponseData = response.data;
      this.list = data.list;
      this.total = data.total;

    } catch (error) {
      this.error = error;
    } finally {
      this.setPending(false);
    }
  }

  @action resetData() {
    this.list = null;
    this.pending = false;
    this.error = null;
    this.offset = defaultOffset;
    this.limit = defaultLimit;
    this.total = 0;
  }

  @action setPending(pendingState: boolean) {
    this.pending = pendingState;
  }

  @action setValues(values: any) {
    set(this, values);
  }
}
