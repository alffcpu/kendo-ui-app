import { action, observable, computed } from 'mobx';

export interface IAuthStore {
  authorized: boolean;
  user: IUser;
  setUser(user: IUser);
  setAuth(authState: boolean);
  isAuthorized();
}

export interface IUser {
  login: string;
  name: string;
  lastName: string;
  role: string;
}

export default class AuthStore {
  @observable authorized = false;
  @observable user: IUser = null;

  @action setUser(user: IUser) {
    this.user = user;
  }

  @action setAuth(authState: boolean) {
    this.authorized = authState;
  }

  @computed get isAuthorized() {
    return this.authorized;
  }
}
