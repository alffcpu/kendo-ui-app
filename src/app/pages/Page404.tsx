import React from 'react';

const Page404: React.SFC = () =>
  <React.Fragment>
    <h1>Страница не найдена!</h1>
  </React.Fragment>;

export default Page404;
