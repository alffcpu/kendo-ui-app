import React from 'react';
import { inject, observer } from 'mobx-react';
import { RouterStore } from 'mobx-react-router';
import { Grid, GridColumn } from '@progress/kendo-react-grid';
import { IArticlesStore } from 'app/stores/ArticlesStore';
import Spinner from 'app/components/Spinner';

interface IProps {
  router?: RouterStore;
  articles?: IArticlesStore;
}

@inject('router', 'articles')
@observer
export default class ArticlesPage extends React.Component<IProps> {

  componentDidMount() {
    const {router, articles} = this.props;
    if (router.location.pathname === '/') {
      router.replace('/articles');
    }
    articles.fetchData();
  }

  // или не делать этого, для сохранения данных
  componentWillUnmount() {
    this.props.articles.resetData();
  }

  handlePageChange = ({page}) => {
    const {articles} = this.props;
    articles.setValues({
      limit: page.take,
      offset: page.skip
    });
    articles.fetchData();
  }

  render() {
    const {list, pending, offset, limit, total} = this.props.articles;
    return (pending && !list ?
      <Spinner /> :
      <React.Fragment>
        <h1>Коды регионов</h1>
        <Grid
            data={list ? [...list] : []}
            skip={offset}
            take={limit}
            total={total}
            onPageChange={this.handlePageChange}
            pageable={{
              buttonCount: 10,
              type: 'numeric',
              pageSizes: true,
              previousNext: true
            }}
            pageSize={limit}
        >
          <GridColumn field="count" title="Номер строки" width="100px" />
          <GridColumn field="title" title="Название региона" />
          <GridColumn field="code" title="Почтовые коды" />
        </Grid>
      </React.Fragment>
    );
  }
}
