import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import { RouterStore } from 'mobx-react-router';

import AuthorizationForm from 'app/components/AuthorizationForm';
import ArticlesPage from 'app/pages/ArticlesPage';
import Page404 from 'app/pages/Page404';

interface IProps {
  router?: RouterStore;
}

@inject('router')
@observer
export default class Routes extends React.Component<IProps> {

  render() {
    const {router} = this.props;
    return (
      <Switch location={router.location}>
        <Route exact path="/" component={ArticlesPage} />
        <Route exact path="/login" component={AuthorizationForm} />
        <Route exact path="/articles" component={ArticlesPage} />
        <Route component={Page404} />
      </Switch>
    );
  }
}
