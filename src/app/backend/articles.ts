import { postRequest } from './request';

export const postArticles = (data = null, config = {withCredentials: false}) =>
  postRequest(`articles`, data, config);
