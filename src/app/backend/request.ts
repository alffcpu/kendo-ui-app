import axios, { AxiosPromise } from 'axios';

const apiUrl = (url) => `http://show.thesuper.ru/${url}`;

export const getRequest = (path, config = null): AxiosPromise =>
  axios.get(apiUrl(path), config);

export const postRequest = (path, data = {}, config = null): AxiosPromise =>
  axios.post(apiUrl(path), data, config);

export const putRequest = (path, data = {}, config = null): AxiosPromise =>
  axios.put(apiUrl(path), data, config);

export const deleteRequest = (path, config = null): AxiosPromise =>
  axios.delete(apiUrl(path), config);

export const isCancel = axios.isCancel;
export const CancelToken = axios.CancelToken;
