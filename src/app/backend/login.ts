import { postRequest } from './request';

export const postAuth = (data = null, config = {withCredentials: false}) =>
  postRequest(`auth`, data, config);
