import React from 'react';
import { createBrowserHistory } from 'history';
import { Provider } from 'mobx-react';
import { RouterStore, syncHistoryWithStore } from 'mobx-react-router';
import { Router } from 'react-router-dom';
import AuthStore from 'app/stores/AuthStore';
import ArticlesStore from 'app/stores/ArticlesStore';
// import { Grid, GridColumn } from '@progress/kendo-react-grid';
import Authentication from 'src/app/components/Authentication';

const routerStore = new RouterStore();

const browserHistory = createBrowserHistory({ basename: '/' });
const history = syncHistoryWithStore(browserHistory, routerStore);

const stores = {
  auth: new AuthStore(),
  router: routerStore,
  articles: new ArticlesStore(),
};

export default class App extends React.Component {

  render() {
    return (
      <React.Fragment>
        <Provider {...stores}>
          <Router history={history}>
            <div className="kendo-app">
              <div className="container">
                <Authentication />
              </div>
            </div>
          </Router>
        </Provider>
      </React.Fragment>
    );
  }
}
