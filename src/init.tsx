// Globals
import 'assets/styles/application.less';
import 'babel-polyfill';
import 'isomorphic-fetch';
import 'bootstrap-4-grid/css/grid.min.css';
import '@progress/kendo-theme-default/dist/all.css';

import React from 'react';
import ReactDOM from 'react-dom';
// import { enableLogging } from 'mobx-logger';

import App from 'app/App';

const root = document.createElement('div');
root.setAttribute('id', 'root');
document.body.insertBefore(root, document.body.children[0]);
ReactDOM.render(<App />, root);
